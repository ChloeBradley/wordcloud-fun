I did not write the code so much as edit a preexisting file, and I changed the directory structure so the script won't run as-is.

But this was a fun project and made for some cute images, which can be seen in src/ims

I plan to do this for other game scripts I get my hands on. Speaking of, the script directories were omitted from this repository.

![Atelier Firis System Text](src/ims/wc-sys.png)