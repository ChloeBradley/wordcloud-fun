#!/usr/bin/env python
"""
Masked wordcloud
================
Using a mask you can generate wordclouds in arbitrary shapes.
"""

from os import path
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import codecs

from wordcloud import WordCloud, STOPWORDS

d = os.getcwd()

# Read the whole text.
text = ""
for filename in glob.iglob(os.getcwd() + '/System Strings' + '/**/*.txt', recursive=True):
    print(filename)
    text += "\r\n" + codecs.open(filename, 'r', 'utf-8').read()
    #text += "\r\n" + open(filename).read()
#text = open(path.join(d, 'alice.txt')).read()

alice_mask = np.array(Image.open(path.join(d, "suppmask.png")))

stopwords = set(STOPWORDS)
#stopwords.add("said")

wc = WordCloud(background_color="white", max_words=2000, mask=alice_mask,
               stopwords=stopwords)
# generate word cloud
wc.generate(text)

# store to file
wc.to_file(path.join(d, "wc.png"))

# show
plt.imshow(wc, interpolation='bilinear')
plt.axis("off")
plt.figure()
plt.imshow(alice_mask, cmap=plt.cm.gray, interpolation='bilinear')
plt.axis("off")
plt.show()